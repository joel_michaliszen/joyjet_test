using System.Collections.Generic;
using JoyJet.eCommerce.Core.DTO;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eCommerce.Core.Interfaces.Services
{
    public interface ICartService
    {
        void AddItemToCart(long cartId, CartItem item);
        IEnumerable<Cart> GetAllCarts();
        Cart IncludeCart(Cart cart);
        IEnumerable<CartCheckoutDTO> GetCartsPreparedToCheckout();
    }
}