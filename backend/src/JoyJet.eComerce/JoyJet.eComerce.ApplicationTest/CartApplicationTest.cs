﻿using JoyJet.eComerce.Application.AppServices;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;
using Ninject;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace JoyJet.eComerce.ApplicationTest
{
    [TestFixture]
    public class CartApplicationTest : TestBase
    {
        private CartApplication _sut;
        private ICartService _cartService;

        [SetUp]
        public void Setup()
        {
            _cartService = _kernel.Get<ICartService>();
            _sut = _kernel.Get<CartApplication>();
        }

        [Test]
        public void OnGetCartsTheGetAllCartsOfCartServiceShouldBeCalledAndTheResultMustBeMappedToCartViewModel()
        {
            var expectedCart = new Cart
            {
                Id = 1,
                Items = new List<CartItem> { new CartItem { ArticleId = 1, Quantity = 2 } }
            };
            _cartService.GetAllCarts()
                .Returns(new[] { expectedCart });
            var assert = _sut.GetCarts();
            _cartService.Received().GetAllCarts();

            Assert.IsTrue(assert.All(e => e.Id == expectedCart.Id), "Some carts are not present in result!");
            Assert.IsTrue(assert.All(e => e.Items.All(x => x.ArticleId == expectedCart.Items.First().ArticleId && x.Quantity == expectedCart.Items.First().Quantity)), "Some itens of cart are not present in the result!");

        }

        [Test]
        public void OnAddCartTheIncludeCartOfCartServiceShouldBeCalled()
        {
            _sut.AddCart(new CartViewModel());
            _cartService.IncludeCart(Arg.Any<Cart>());
        }

        [TestCase(666)]
        public void OnAddItemToCartTheAddItemToCartOfCartServiceShouldBeCalled(long cartId)
        {
            _sut.AddItemToCart(cartId, new CartItemViewModel());
            _cartService.Received().AddItemToCart(cartId, Arg.Any<CartItem>());
        }
    }
}
