using System.Collections.Generic;
using JoyJet.eComerce.Application.ViewModels;

namespace JoyJet.eComerce.Application.Interfaces
{
    public interface IArticleApplication
    {
        IEnumerable<ArticleViewModel> GettAllArticles();
    }
}