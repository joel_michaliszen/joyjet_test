﻿
using Ninject.MockingKernel.NSubstitute;
using NUnit.Framework;

namespace JoyJet.eComerce.CoreTest
{
    public abstract class TestBase
    {
        protected NSubstituteMockingKernel _kernel;

        protected TestBase()
        {
            _kernel = new NSubstituteMockingKernel();
        }

        [SetUp]
        protected void SetupBase()
            => _kernel.Reset();
    }
}
