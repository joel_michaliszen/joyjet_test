﻿using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.EntityConfigurations.Common;

namespace JoyJet.eCommerce.Data.EntityConfigurations
{
    public class DiscountEntityTypeConfiguration : EntityBaseEntityTypeConfiguration<Discount>
    {
        public DiscountEntityTypeConfiguration()
        {
            HasRequired(e => e.Article).WithMany().HasForeignKey(e => e.ArticleId).WillCascadeOnDelete(true);
            Property(e => e.Value).IsRequired();
            Property(e => e.Type).IsRequired();
        }
    }
}
