﻿using AutoMapper;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.DTO;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eComerce.Application.Profiles
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMap<CartItem, CartItemViewModel>();
            CreateMap<CartItemViewModel, CartItem>();
            CreateMap<Cart, CartViewModel>();
            CreateMap<CartViewModel, Cart>();

            CreateMap<CartCheckoutDTO, CartCheckoutViewModel>();
        }
    }
}
