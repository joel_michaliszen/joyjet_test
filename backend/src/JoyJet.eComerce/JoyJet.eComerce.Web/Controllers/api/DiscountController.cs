﻿using System.Collections.Generic;
using System.Web.Http;
using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;

namespace JoyJet.eComerce.Web.Controllers.api
{
    [RoutePrefix("api/discount")]
    public class DiscountController : ApiController
    {
        private readonly IDiscountApplication _discountApplication;

        public DiscountController(IDiscountApplication discountApplication)
        {
            _discountApplication = discountApplication;
        }

        public IEnumerable<DiscountViewModel> Get()
            => _discountApplication.GetDiscounts();


        public DiscountViewModel Post(DiscountViewModel discount)
            => _discountApplication.CreateDiscount(discount);
    }
}
