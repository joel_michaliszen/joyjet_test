using System.Collections.Generic;

namespace JoyJet.eComerce.Application.ViewModels
{
    public class CartViewModel
    {
        public long Id { get; set; }
        public IEnumerable<CartItemViewModel> Items { get; set; }
    }
}