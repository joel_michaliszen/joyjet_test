﻿using JoyJet.eComerce.Application.AppServices;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data;
using Ninject.Extensions.Conventions;
using Ninject.Modules;
using Ninject.Web.Common;
using static System.Reflection.Assembly;

namespace JoyJet.eComerce.IoC
{
    public class WebDefaultModule : NinjectModule
    {
        public override void Load()
            => Kernel.Bind(c => c.From(GetAssembly(typeof(IUnitOfWork)), GetAssembly(typeof(ArticleApplication)), GetAssembly(typeof(Article)))
                        .SelectAllClasses()
                        .BindDefaultInterfaces()
                        .Configure(ctx => ctx.InRequestScope()));
    }
}