namespace JoyJet.eCommerce.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingCart : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cart",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CartItem",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ArticleId = c.Long(nullable: false),
                        CartId = c.Long(nullable: false),
                        Quantity = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Article", t => t.ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.Cart", t => t.CartId, cascadeDelete: true)
                .Index(t => t.ArticleId)
                .Index(t => t.CartId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CartItem", "CartId", "dbo.Cart");
            DropForeignKey("dbo.CartItem", "ArticleId", "dbo.Article");
            DropIndex("dbo.CartItem", new[] { "CartId" });
            DropIndex("dbo.CartItem", new[] { "ArticleId" });
            DropTable("dbo.CartItem");
            DropTable("dbo.Cart");
        }
    }
}
