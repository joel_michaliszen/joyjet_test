﻿using System.Reflection;
using AutoMapper;

namespace JoyJet.eComerce.Application.Profiles
{
    public static class MapperStart
    {
        public static void Configure()
            => Mapper.Initialize(x => x.AddProfiles(Assembly.GetExecutingAssembly()));
    }
}
