﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Interfaces.Services;

namespace JoyJet.eComerce.Application.AppServices
{
    public class DeliveryFeeApplication : IDeliveryFeeApplication
    {
        private readonly IDeliveryFeeService _deliveryFeeService;

        public DeliveryFeeApplication(IDeliveryFeeService deliveryFeeService)
        {
            _deliveryFeeService = deliveryFeeService;
        }

        public IEnumerable<DeliveryFeeViewModel> GetAllDeliveryFees()
            => _deliveryFeeService.GetAllDeliveryFees()?.ToList()?.AsQueryable().ProjectTo<DeliveryFeeViewModel>();
    }
}
