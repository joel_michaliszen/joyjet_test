namespace JoyJet.eComerce.Application.ViewModels
{
    public class CartCheckoutViewModel
    {
        public long Id { get; set; }
        public long Total { get; set; }
    }
}