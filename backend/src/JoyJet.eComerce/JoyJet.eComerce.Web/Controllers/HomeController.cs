﻿using System.Web.Mvc;

namespace JoyJet.eComerce.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index() => Redirect("swagger/ui/index");
    }
}