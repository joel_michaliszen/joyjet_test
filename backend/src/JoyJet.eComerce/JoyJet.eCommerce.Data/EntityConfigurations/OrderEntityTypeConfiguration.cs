﻿using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.EntityConfigurations.Common;

namespace JoyJet.eCommerce.Data.EntityConfigurations
{
    public class OrderEntityTypeConfiguration : EntityBaseEntityTypeConfiguration<Order>
    {
        public OrderEntityTypeConfiguration()
        {
            Property(e => e.CheckoutAt);
            Property(e => e.DeliveryNextDay);
        }
    }
}
