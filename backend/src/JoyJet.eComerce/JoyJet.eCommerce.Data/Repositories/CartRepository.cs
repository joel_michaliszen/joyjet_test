﻿using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.Repositories.Commom;
using System.Data.Entity;
using System.Linq;

namespace JoyJet.eCommerce.Data.Repositories
{
    public class CartRepository : Repository<Cart>, ICartRepository
    {
        public CartRepository(IUnitOfWork uow) : base(uow)
        { }

        public override IQueryable<Cart> List()
            => base.List().Include(e => e.Items)
            .Include("Items.Article")
            .AsNoTracking();
    }
}
