﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using JoyJet.eComerce.Web.Models;
using JoyJet.eCommerce.CrossCutting.Exceptions;

namespace JoyJet.eComerce.Web.Filters
{
    public class ExceptionToStatusCodeFilter : IExceptionFilter
    {
        public bool AllowMultiple { get; }

        public async Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            if (actionExecutedContext.Exception is JoyJetECommerceException)
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new ErrorMessage(actionExecutedContext.Exception.Message));
            await Task.Run(() => { }, cancellationToken);
        }

    }
}