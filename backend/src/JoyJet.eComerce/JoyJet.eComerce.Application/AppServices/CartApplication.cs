﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eComerce.Application.AppServices
{
    public class CartApplication : ICartApplication
    {
        private readonly ICartService _cartService;

        public CartApplication(ICartService cartService)
        {
            _cartService = cartService;
        }

        public IEnumerable<CartViewModel> GetCarts()
            => _cartService.GetAllCarts()?.AsQueryable().ProjectTo<CartViewModel>();

        public CartViewModel AddCart(CartViewModel cart)
        {
            var included = _cartService.IncludeCart(Mapper.Map<Cart>(cart));
            return Mapper.Map<CartViewModel>(included);
        }

        public IEnumerable<CartCheckoutViewModel> GetCartsToCheckout()
            => _cartService.GetCartsPreparedToCheckout().AsQueryable().ProjectTo<CartCheckoutViewModel>();

        public void AddItemToCart(long cartId, CartItemViewModel cartItem)
            => _cartService.AddItemToCart(cartId, Mapper.Map<CartItem>(cartItem));
    }
}
