﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using JoyJet.eCommerce.Data.Migrations;

namespace JoyJet.eCommerce.Data
{
    public class JJContext : DbContext
    {
        public JJContext() : base("JJConnStr")
        {
            //Workaround to copy the sql provider dll to the output
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<JJContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(typeof(JJContext).Assembly);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
