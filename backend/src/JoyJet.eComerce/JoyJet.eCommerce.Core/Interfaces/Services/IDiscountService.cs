using System.Collections.Generic;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eCommerce.Core.Interfaces.Services
{
    public interface IDiscountService
    {
        IEnumerable<Discount> GetAllDiscount();
        Discount IncludeDiscount(Discount discount);
    }
}