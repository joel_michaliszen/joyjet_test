﻿
using JoyJet.eCommerce.Core.DTO;
using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Core.Services;
using JoyJet.eCommerce.CrossCutting.Exceptions;
using Ninject;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace JoyJet.eComerce.CoreTest
{
    [TestFixture]
    public class CartServiceTest : TestBase
    {
        private CartService _sut;
        private IRepository<Article> _articleRepository;
        private ICartRepository _cartRepository;
        private IRepository<DeliveryFee> _deliveryFeeRepository;

        [SetUp]
        public void Setup()
        {
            _articleRepository = _kernel.Get<IRepository<Article>>();
            _deliveryFeeRepository = _kernel.Get<IRepository<DeliveryFee>>();
            _cartRepository = _kernel.Get<ICartRepository>();
            _sut = _kernel.Get<CartService>();
        }

        [Test]
        public void OnCallgetAllCartsListOfcartRepositoryShouldBeCalled()
        {
            _sut.GetAllCarts();
            _cartRepository.Received().List();
        }

        [Test]
        public void OnCallingIncludeCartInclueOfcartRepositoryShouldBeCalled()
        {
            var expected = new Cart();
            _sut.IncludeCart(expected);
            _cartRepository.Received().Include(expected);
        }

        [TestCase("Cart item article doesn't exists!")]
        public void OnCallingIncludeCartIfArticleInTheItemDoesntNotExistsAnExceptionShouldBeThrowledWithMessage(string expectedMessage)
        {
            var expected = new Cart { Items = new List<CartItem> { new CartItem() } };
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.IncludeCart(expected));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [TestCase("Cart item quantity must be bigger than zero!")]
        public void OnCallingIncludeCartIfQuantityIsLessThanZeroAnExceptionShouldBeThrowledWithMessage(string expectedMessage)
        {
            var expected = new Cart { Items = new List<CartItem> { new CartItem() } };
            _articleRepository.Exists(Arg.Any<long>()).Returns(true);
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.IncludeCart(expected));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [TestCase("Cart item article doesn't exists!")]
        public void OnAddItemToChartIfArticleInTheItemDoesntNotExistsAnExceptionShouldBeThrowledWithMessage(string expectedMessage)
        {
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.AddItemToCart(999, new CartItem()));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [TestCase("Cart item quantity must be bigger than zero!")]
        public void OnAddItemToChartIfQuantityIsLessThanZeroAnExceptionShouldBeThrowledWithMessage(string expectedMessage)
        {
            var cartItem = new CartItem
            {
                ArticleId = 1,
                Quantity = -1
            };
            _articleRepository.Exists(cartItem.ArticleId).Returns(true);
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.AddItemToCart(999, cartItem));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [Test]
        public void OnAddItemToChartTheUpdateOfTheCartRepositoryMostBeCalledWithCartWithTheNewItemAsParam()
        {
            var cartItem = new CartItem
            {
                ArticleId = 1,
                Quantity = 1
            };
            _articleRepository.Exists(cartItem.ArticleId).Returns(true);
            var expectedCart = new Cart { Id = 1 };
            _cartRepository.Find(1).Returns(expectedCart);
            _sut.AddItemToCart(expectedCart.Id, cartItem);
            Assert.AreEqual(1, expectedCart.Items.Count);
            _cartRepository.Received().Update(Arg.Is<Cart>(e => e.Id == expectedCart.Id && e.Items.Any()));

        }

        //Level -1
        [Test]
        public void OnGetCartsPreparedToCheckoutGivenIHaveAListOfCartsAndDoesntExistsDeliveryFeesAndDiscountsShouldReturnCorrectTotal()
        {
            PrepareTestData(withDeliveryFee: false);
            var expected = GetExpectedResult(2000, 1400, 0);
            var result = _sut.GetCartsPreparedToCheckout().ToList();
            Assert.AreEqual(expected.Sum(e => e.Total), result.Sum(e => e.Total));
        }

        //Level - 2
        [Test]
        public void OnGetCartsPreparedToCheckoutGivenIHaveAListOfCartsAndAListOfElegibleRulesToDeliveryFeesTheResultShouldContainTheSumOfPriceAndFee()
        {
            PrepareTestData(true);
            var expected = GetExpectedResult(2000, 1800, 800);
            var result = _sut.GetCartsPreparedToCheckout().ToList();
            Assert.AreEqual(expected.Sum(e => e.Total), result.Sum(e => e.Total));
        }


        //Level - 3
        [Test]
        public void OnGetCartsPreparedToCheckoutGivenIHaveAListOfCartsAndAListOfElegibleRulesToDeliveryFeesAndItemsHaveDiscountTheResultShouldContainTheSumOfPriceMinusDiscountAndFee()
        {
            var expected = GetExpectedResult(2350, 1775, 1798, 1083, 1196);
            _cartRepository.List().Returns(GetCartsWithArticlesDiscounts().AsQueryable());
            _deliveryFeeRepository.List().Returns(GetDefaultDeliveryFees().AsQueryable());
            var result = _sut.GetCartsPreparedToCheckout().ToList();
            var expectedSum = expected.Sum(e => e.Total);
            var resultSum = result.Sum(e => e.Total);
            Assert.AreEqual(expectedSum, resultSum);
        }

        private void PrepareTestData(bool withDeliveryFee)
        {
            _cartRepository.List().Returns(GetDefaultCarts().AsQueryable());
            if (withDeliveryFee)
                _deliveryFeeRepository.List().Returns(GetDefaultDeliveryFees().AsQueryable());
        }

        private static IEnumerable<CartCheckoutDTO> GetExpectedResult(params long[] totals)
            => totals.Select(total => new CartCheckoutDTO { Total = total });

        public IEnumerable<DeliveryFee> GetDefaultDeliveryFees() => new List<DeliveryFee>
        {
            new DeliveryFee
            {
                EligibleTransactionVolume = new EligibleTransactionVolume
                {
                    MinPrice = 0,
                    MaxPrice = 1000,
                },
                Price = 800
            },
            new DeliveryFee
            {
                EligibleTransactionVolume = new EligibleTransactionVolume
                {
                    MinPrice = 1000,
                    MaxPrice = 2000,
                },
                Price = 400
            },
            new DeliveryFee
            {
                EligibleTransactionVolume = new EligibleTransactionVolume
                {
                    MinPrice = 2000,
                    MaxPrice = null,
                },
                Price = 0
            },
        };

        public IEnumerable<Cart> GetCartsWithArticlesDiscounts()
        {
            var article2 = new Article { Price = 200, DiscountId = 666, Discount = new Discount { Type = DiscountType.Amount, Value = 25 } };
            var article5And6 = new Article { Price = 999, DiscountId = 666, Discount = new Discount { Type = DiscountType.Percentage, Value = 30 } };
            var article7 = new Article { Price = 378, DiscountId = 666, Discount = new Discount { Type = DiscountType.Percentage, Value = 25 } };
            var article8 = new Article { Price = 147, DiscountId = 666, Discount = new Discount { Type = DiscountType.Percentage, Value = 10 } };
            return new List<Cart>
            {
                new Cart
                {
                    Id = 1,
                    Items = new List<CartItem>
                    {
                        new CartItem { Article = new Article {Price = 100}, Quantity = 6},
                        new CartItem { Article = article2, Quantity = 2},
                        new CartItem { Article = new Article {Price = 1000}, Quantity = 1},
                    }
                },
                new Cart
                {
                    Id = 2,
                    Items = new List<CartItem>
                    {
                        new CartItem { Article = article2, Quantity = 1},
                        new CartItem { Article = new Article {Price = 400}, Quantity = 3},
                    }
                },
                new Cart
                {
                    Id = 3,
                    Items = new List<CartItem>
                    {
                        new CartItem { Article = article5And6, Quantity = 1},
                        new CartItem { Article = article5And6, Quantity = 1},
                    }
                },
                new Cart
                {
                    Id = 4,
                    Items = new List<CartItem>
                    {
                        new CartItem { Article = article7, Quantity = 1},

                    }
                },
                new Cart
                {
                    Id = 5,
                    Items = new List<CartItem>
                    {
                        new CartItem { Article = article8, Quantity = 3},
                    }
                }
            };
        }

        public IEnumerable<Cart> GetDefaultCarts()
        {
            return new List<Cart>
            {
                new Cart
                {
                    Id = 1,
                    Items = new List<CartItem>
                    {
                        new CartItem { Article = new Article {Price = 100}, Quantity = 6},
                        new CartItem { Article = new Article {Price = 200}, Quantity = 2},
                        new CartItem { Article = new Article {Price = 1000}, Quantity = 1},
                    }
                },
                new Cart
                {
                    Id = 2,
                    Items = new List<CartItem>
                    {
                        new CartItem { Article = new Article {Price = 200}, Quantity = 1},
                        new CartItem { Article = new Article {Price = 400}, Quantity = 3},
                    }
                },
                new Cart
                {
                    Id = 3,
                }
            };
        }
    }
}
