﻿namespace JoyJet.eCommerce.Core.Models.Common
{
    public abstract class EntityBase
    {
        public long Id { get; set; }
    }
}
