﻿using System;
using System.Data.Entity;

namespace JoyJet.eCommerce.Data
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext Context { get; }

        void StartTransaction();

        void Commit();

        void Save();

        void Rollback();
    }
}
