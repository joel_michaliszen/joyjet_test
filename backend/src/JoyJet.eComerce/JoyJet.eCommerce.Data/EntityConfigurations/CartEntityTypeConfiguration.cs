﻿using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.EntityConfigurations.Common;

namespace JoyJet.eCommerce.Data.EntityConfigurations
{
    public class CartEntityTypeConfiguration : EntityBaseEntityTypeConfiguration<Cart>
    {
    }
}
