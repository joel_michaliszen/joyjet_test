﻿using System.Collections.Generic;
using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eCommerce.Core.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IRepository<Article> _articleRepository;

        public ArticleService(IRepository<Article> articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IEnumerable<Article> GetAllArticles()
            => _articleRepository.List();

        public Article AddArticle(Article article)
            => _articleRepository.Include(article);
    }
}
