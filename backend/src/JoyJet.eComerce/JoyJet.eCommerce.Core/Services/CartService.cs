﻿using System.Collections.Generic;
using System.Linq;
using JoyJet.eCommerce.Core.DTO;
using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.CrossCutting.Exceptions;

namespace JoyJet.eCommerce.Core.Services
{
    public class CartService : ICartService
    {
        private readonly ICartRepository _cartRepository;
        private readonly IRepository<Article> _articleRepository;
        private readonly IRepository<DeliveryFee> _deliveryFeeRepository;

        public CartService(ICartRepository cartRepository, IRepository<Article> articleRepository, IRepository<DeliveryFee> deliveryFeeRepository)
        {
            _cartRepository = cartRepository;
            _articleRepository = articleRepository;
            _deliveryFeeRepository = deliveryFeeRepository;
        }

        public IEnumerable<Cart> GetAllCarts()
            => _cartRepository.List();

        public IEnumerable<CartCheckoutDTO> GetCartsPreparedToCheckout()
        {
            var query = from cart in _cartRepository.List().ToList()
                        let total = cart.Items != null && cart.Items.Any() ? cart.Items.Sum(e => e.Price()) : 0
                        let fee = _deliveryFeeRepository.List().FirstOrDefault(e => e.EligibleTransactionVolume.MinPrice <= total &&
                                                                               (e.EligibleTransactionVolume.MaxPrice == null ||
                                                                                e.EligibleTransactionVolume.MaxPrice > total))
                        let feePrice = fee?.Price ?? 0
                        select new CartCheckoutDTO
                        {
                            Id = cart.Id,
                            Total = total + feePrice
                        };

            return query;
        }

        public Cart IncludeCart(Cart cart)
        {
            PrepareCartItems(cart);
            if (cart.Items?.Any() ?? false)
                foreach (var cartItem in cart.Items)
                    ValidateItem(cartItem);

            return _cartRepository.Include(cart);
        }

        public void AddItemToCart(long cartId, CartItem item)
        {
            ValidateItem(item);
            var cart = _cartRepository.Find(cartId);
            ValidateCart(cart);
            cart.Items = cart.Items ?? new List<CartItem>();
            PrepareNewCartItem(item, cart);
            _cartRepository.Update(cart);
        }

        private void ValidateItem(CartItem item)
        {
            if (!_articleRepository.Exists(item.ArticleId))
                throw new JoyJetECommerceException("Cart item article doesn't exists!");
            if (item.Quantity <= 0)
                throw new JoyJetECommerceException("Cart item quantity must be bigger than zero!");
        }

        private static void ValidateCart(Cart cart)
        {
            if (cart == null)
                throw new JoyJetECommerceException("Cart not found!");
        }

        private static void PrepareNewCartItem(CartItem item, Cart cart)
        {
            if (cart.Items.Any(e => e.ArticleId == item.ArticleId))
                cart.Items.FirstOrDefault(e => e.ArticleId == item.ArticleId).Quantity += item.Quantity;
            else
                cart.Items.Add(item);
        }

        private static void PrepareCartItems(Cart cart)
            => cart.Items = cart.Items?.GroupBy(e => e.ArticleId)
                                    .Select(grp => new CartItem { ArticleId = grp.Key, Quantity = grp.Sum(e => e.Quantity) })
                                    .ToList();
    }
}
