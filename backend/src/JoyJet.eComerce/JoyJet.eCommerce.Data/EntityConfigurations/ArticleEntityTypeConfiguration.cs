﻿using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.EntityConfigurations.Common;

namespace JoyJet.eCommerce.Data.EntityConfigurations
{
    public class ArticleEntityTypeConfiguration : EntityBaseEntityTypeConfiguration<Article>
    {
        public ArticleEntityTypeConfiguration()
        {
            Property(e => e.Name).IsRequired();
            Property(e => e.Price);
            HasOptional(e => e.Discount).WithMany().HasForeignKey(e => e.DiscountId);
        }
    }
}
