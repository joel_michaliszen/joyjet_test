﻿using System.Linq;
using JoyJet.eCommerce.Core.Models.Common;

namespace JoyJet.eCommerce.Core.Interfaces.Repositories
{
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        TEntity Find(long id);

        IQueryable<TEntity> List();

        TEntity Include(TEntity entity);

        TEntity Update(TEntity entity);

        void Delete(long id);

        void Delete(TEntity entity);
        bool Exists(long id);
    }
}
