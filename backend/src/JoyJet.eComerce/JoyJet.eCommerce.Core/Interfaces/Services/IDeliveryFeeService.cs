using System.Collections.Generic;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eCommerce.Core.Interfaces.Services
{
    public interface IDeliveryFeeService
    {
        IEnumerable<DeliveryFee> GetAllDeliveryFees();
        long GetFeeByPrice(long cartPrice);
    }
}