using AutoMapper;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eComerce.Application.Profiles
{
    public class DeliveryFeeProfile : Profile
    {
        public DeliveryFeeProfile()
        {
            CreateMap<DeliveryFee, DeliveryFeeViewModel>()
                .ForMember(to => to.EligibleTransactionVolume, f => f.MapFrom(from => Mapper.Map<EligibleTransactionVolumeViewModel>(from.EligibleTransactionVolume)));
            CreateMap<EligibleTransactionVolume, EligibleTransactionVolumeViewModel>();
        }
    }
}