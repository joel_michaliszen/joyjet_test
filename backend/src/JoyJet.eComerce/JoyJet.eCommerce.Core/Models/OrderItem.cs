using JoyJet.eCommerce.Core.Models.Common;

namespace JoyJet.eCommerce.Core.Models
{
    public class OrderItem : EntityBase
    {
        public long OrderId { get; set; }
        public virtual Order Order { get; set; }

        public long ArticleId { get; set; }
        public virtual Article Article { get; set; }

        public int Quantity { get; set; }
    }
}