﻿using System;

namespace JoyJet.eCommerce.CrossCutting.Exceptions
{
    public class JoyJetECommerceException : Exception
    {
        public JoyJetECommerceException()
        { }

        public JoyJetECommerceException(string msg) : base(msg)
        { }

        public JoyJetECommerceException(string msg, Exception inner) : base(msg, inner)
        { }
    }
}
