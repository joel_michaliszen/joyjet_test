﻿using System.Collections.Generic;
using System.Web.Http;
using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;

namespace JoyJet.eComerce.Web.Controllers.api
{
    [RoutePrefix("api/deliveryfee")]
    public class DeliveryFeeController : ApiController
    {
        private readonly IDeliveryFeeApplication _deliveryFeeApplication;

        public DeliveryFeeController(IDeliveryFeeApplication deliveryFeeApplication)
        {
            _deliveryFeeApplication = deliveryFeeApplication;
        }

        public IEnumerable<DeliveryFeeViewModel> Get()
            => _deliveryFeeApplication.GetAllDeliveryFees();
    }
}
