namespace JoyJet.eComerce.Application.ViewModels
{
    public class EligibleTransactionVolumeViewModel
    {
        public long? MinPrice { get; set; }
        public long? MaxPrice { get; set; }
    }
}