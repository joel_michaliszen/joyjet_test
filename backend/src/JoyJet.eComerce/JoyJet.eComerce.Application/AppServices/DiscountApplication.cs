﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eComerce.Application.AppServices
{
    public class DiscountApplication : IDiscountApplication
    {
        private readonly IDiscountService _discountService;

        public DiscountApplication(IDiscountService discountService)
        {
            _discountService = discountService;
        }

        public IEnumerable<DiscountViewModel> GetDiscounts()
            => _discountService.GetAllDiscount().AsQueryable().ProjectTo<DiscountViewModel>();

        public DiscountViewModel CreateDiscount(DiscountViewModel discount)
        {
            var created = _discountService.IncludeDiscount(Mapper.Map<Discount>(discount));
            return Mapper.Map<DiscountViewModel>(created);
        }
    }
}
