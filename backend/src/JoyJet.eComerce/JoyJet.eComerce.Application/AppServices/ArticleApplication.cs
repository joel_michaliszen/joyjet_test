﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Interfaces.Services;

namespace JoyJet.eComerce.Application.AppServices
{
    public class ArticleApplication : IArticleApplication
    {
        private readonly IArticleService _articleService;

        public ArticleApplication(IArticleService articleService)
        {
            _articleService = articleService;
        }

        public IEnumerable<ArticleViewModel> GettAllArticles()
            => _articleService.GetAllArticles()?.AsQueryable().ProjectTo<ArticleViewModel>();

    }
}
