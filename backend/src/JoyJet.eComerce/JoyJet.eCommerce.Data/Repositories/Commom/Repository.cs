﻿using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Models.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace JoyJet.eCommerce.Data.Repositories.Commom
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private readonly IUnitOfWork _uow;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(IUnitOfWork uow)
        {
            _uow = uow;
            _dbSet = uow.Context.Set<TEntity>();
        }

        protected virtual IQueryable<TEntity> DefaultQuery => _dbSet;

        public virtual TEntity Find(long id)
            => _dbSet.Find(id);

        public virtual bool Exists(long id)
            => _dbSet.Find(id) != null;

        public virtual IQueryable<TEntity> List() => DefaultQuery;

        public virtual TEntity Include(TEntity entity)
        {
            _dbSet.AddOrUpdate(entity);
            _uow.Save();
            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            _dbSet.AddOrUpdate(entity);
            _uow.Save();
            return entity;
        }

        public virtual void Delete(long id)
            => Delete(_dbSet.Find(id));

        public virtual void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
            _uow.Save();
        }
    }
}
