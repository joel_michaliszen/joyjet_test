using System.Collections.Generic;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eCommerce.Core.Interfaces.Services
{
    public interface IArticleService
    {
        IEnumerable<Article> GetAllArticles();
        Article AddArticle(Article article);
    }
}