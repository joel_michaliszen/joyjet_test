﻿using System.Collections.Generic;
using JoyJet.eComerce.Application.ViewModels;

namespace JoyJet.eComerce.Application.Interfaces
{
    public interface IDiscountApplication
    {
        DiscountViewModel CreateDiscount(DiscountViewModel discount);
        IEnumerable<DiscountViewModel> GetDiscounts();
    }
}