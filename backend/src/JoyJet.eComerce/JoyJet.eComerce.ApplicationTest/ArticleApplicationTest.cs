﻿using JoyJet.eComerce.Application.AppServices;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;
using Ninject;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace JoyJet.eComerce.ApplicationTest
{
    [TestFixture]
    public class ArticleApplicationTest : TestBase
    {
        private ArticleApplication _sut;
        private IArticleService _articleService;

        [SetUp]
        public void Setup()
        {
            _articleService = _kernel.Get<IArticleService>();
            _sut = _kernel.Get<ArticleApplication>();
        }

        [Test]
        public void OnCallGettAllArticlesTheGetAllArticlesOfArticleServiceIsCalled()
        {
            _sut.GettAllArticles();
            _articleService.Received().GetAllArticles();
        }

        [Test]
        public void OnCallGettAllArticleIfGetAllArticlesFromArticleServicWasEmptyDoesnThrowException()
        {
            _articleService.GetAllArticles().Returns(new List<Article>());
            Assert.DoesNotThrow(() => _sut.GettAllArticles());
        }

        [Test]
        public void OnCallGettAllArticleCanProjectToViewModel()
        {
            var expected = new List<Article>
            {
                new Article { Id = 1, Name = "Test one", Price = 150 },
                new Article { Id = 2, Name = "Teste two", Price = 650 }
            };
            _articleService.GetAllArticles().Returns(expected);
            var assert = _sut.GettAllArticles();
            Assert.IsTrue(assert.All(e => expected.Exists(c => c.Id == e.Id)), "Some articles cannot be mapped. Property: Id ");
            Assert.IsTrue(assert.All(e => expected.Exists(c => c.Name == e.Name)), "Some articles cannot be mapped. Property: Name ");
            Assert.IsTrue(assert.All(e => expected.Exists(c => c.Price == e.Price)), "Some articles cannot be mapped. Property: Price ");
        }
    }
}
