﻿using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Core.Services;
using JoyJet.eCommerce.CrossCutting.Exceptions;
using Ninject;
using NSubstitute;
using NUnit.Framework;

namespace JoyJet.eComerce.CoreTest
{
    [TestFixture]
    public class DiscountServiceTest : TestBase
    {
        private DiscountService _sut;
        private IRepository<Discount> _discountRepository;
        private IRepository<Article> _articleRepository;

        [SetUp]
        public void SetUp()
        {
            _discountRepository = _kernel.Get<IRepository<Discount>>();
            _articleRepository = _kernel.Get<IRepository<Article>>();
            _sut = _kernel.Get<DiscountService>();
        }

        [Test]
        public void OnGetAllDiscountTheListOfDiscountRepositoryshouldBeCalled()
        {
            _sut.GetAllDiscount();
            _discountRepository.Received().List();
        }

        [TestCase("Discount must to have an article!")]
        public void OnIncludeDiscountIfArticleIdWasLessOrEqualsZeroExceptionWithMessageShoudbeThrowled(string expectedMessage)
        {
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.IncludeDiscount(new Discount()));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(101)]
        [TestCase(999)]
        public void OnIncludeDiscountIfPercentageDiscountAndValueWasBiggerThan100OrLessOrEqualsZeroExceptionWithMessageShoudbeThrowled(long value)
        {
            const string expectedMessage = "Discount must to be between 1 and 100!";
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.IncludeDiscount(new Discount { ArticleId = 1, Type = DiscountType.Percentage, Value = value }));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [TestCase("Article doesnt exists.")]
        public void OnIncludeDiscountIfPercentageDiscountAndArticleDoesntExistsExceptionWithMessageShoudbeThrowled(string expectedMessage)
        {
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.IncludeDiscount(new Discount { ArticleId = 1, Type = DiscountType.Percentage, Value = 1 }));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [TestCase("The article  already has a discount!")]
        public void OnIncludeDiscountIfPercentageDiscountAndArticleAlreadyHasDiscountExceptionWithMessageShoudbeThrowled(string expectedMessage)
        {
            _articleRepository.Exists(1).Returns(true);
            _articleRepository.Find(1).Returns(new Article { DiscountId = 1 });
            var ex = Assert.Throws<JoyJetECommerceException>(() => _sut.IncludeDiscount(new Discount { ArticleId = 1, Type = DiscountType.Percentage, Value = 1 }));
            StringAssert.AreEqualIgnoringCase(expectedMessage, ex.Message);
        }

        [Test]
        public void OnIncludeDiscountIfPercentageDiscountIncludeShouldBeCalled()
        {
            _articleRepository.Exists(1).Returns(true);
            var discount = new Discount { ArticleId = 1, Type = DiscountType.Percentage, Value = 1 };
            _discountRepository.Include(discount).Returns(discount);
            _articleRepository.Find(Arg.Any<long>()).Returns(new Article());
            _sut.IncludeDiscount(discount);
            _discountRepository.Received().Include(discount);
        }

        [Test]
        public void OnIncludeDiscountIfPercentageDiscountArticleShoulbeUpatedWithTheDiscountId()
        {
            _articleRepository.Exists(1).Returns(true);
            var discount = new Discount { Id = 1, ArticleId = 1, Type = DiscountType.Percentage, Value = 1 };
            _discountRepository.Include(discount).Returns(discount);
            _articleRepository.Find(Arg.Any<long>()).Returns(new Article());
            _sut.IncludeDiscount(discount);
            _articleRepository.Received().Update(Arg.Is<Article>(x => x.DiscountId == discount.Id));
        }
    }
}
