﻿using JoyJet.eCommerce.Core.Models.Common;

namespace JoyJet.eCommerce.Core.Models
{
    public class Article : EntityBase
    {
        public string Name { get; set; }

        public long Price { get; set; }

        public long? DiscountId { get; set; }
        public virtual Discount Discount { get; set; }
    }
}
