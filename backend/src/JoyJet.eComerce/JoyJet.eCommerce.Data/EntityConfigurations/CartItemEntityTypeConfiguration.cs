﻿using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.EntityConfigurations.Common;

namespace JoyJet.eCommerce.Data.EntityConfigurations
{
    public class CartItemEntityTypeConfiguration : EntityBaseEntityTypeConfiguration<CartItem>
    {
        public CartItemEntityTypeConfiguration()
        {
            HasRequired(e => e.Cart).WithMany(e => e.Items).HasForeignKey(e => e.CartId);
            HasRequired(e => e.Article).WithMany().HasForeignKey(e => e.ArticleId);
        }
    }
}