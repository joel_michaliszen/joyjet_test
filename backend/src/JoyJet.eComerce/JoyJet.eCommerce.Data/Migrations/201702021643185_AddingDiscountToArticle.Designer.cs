// <auto-generated />
namespace JoyJet.eCommerce.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddingDiscountToArticle : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddingDiscountToArticle));
        
        string IMigrationMetadata.Id
        {
            get { return "201702021643185_AddingDiscountToArticle"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
