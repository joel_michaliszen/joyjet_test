﻿using JoyJet.eCommerce.Core.Models.Common;
using System.Collections.Generic;

namespace JoyJet.eCommerce.Core.Models
{
    public class Cart : EntityBase
    {
        public virtual ICollection<CartItem> Items { get; set; }
    }
}
