using System.Collections.Generic;
using JoyJet.eComerce.Application.ViewModels;

namespace JoyJet.eComerce.Application.Interfaces
{
    public interface ICartApplication
    {
        CartViewModel AddCart(CartViewModel cart);
        void AddItemToCart(long cartId, CartItemViewModel cartItem);
        IEnumerable<CartViewModel> GetCarts();
        IEnumerable<CartCheckoutViewModel> GetCartsToCheckout();
    }
}