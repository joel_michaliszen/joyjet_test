﻿using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.EntityConfigurations.Common;

namespace JoyJet.eCommerce.Data.EntityConfigurations
{
    public class DeliveryFeeEntityTypeConfiguration : EntityBaseEntityTypeConfiguration<DeliveryFee>
    {
        public DeliveryFeeEntityTypeConfiguration()
        {
            Property(e => e.EligibleTransactionVolume.MinPrice).IsOptional();
            Property(e => e.EligibleTransactionVolume.MaxPrice).IsOptional();
            Property(e => e.Price).IsRequired();
        }
    }
}
