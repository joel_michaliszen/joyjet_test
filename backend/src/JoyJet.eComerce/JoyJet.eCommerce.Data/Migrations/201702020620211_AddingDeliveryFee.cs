namespace JoyJet.eCommerce.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddingDeliveryFee : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeliveryFee",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    EligibleTransactionVolume_MinPrice = c.Long(),
                    EligibleTransactionVolume_MaxPrice = c.Long(),
                    Price = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropTable("dbo.DeliveryFee");
        }
    }
}
