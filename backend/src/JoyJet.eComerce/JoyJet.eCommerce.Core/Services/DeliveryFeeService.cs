﻿using System.Collections.Generic;
using System.Linq;
using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eCommerce.Core.Services
{
    public class DeliveryFeeService : IDeliveryFeeService
    {
        private readonly IRepository<DeliveryFee> _deliveryFeeRepository;

        public DeliveryFeeService(IRepository<DeliveryFee> deliveryFeeRepository)
        {
            _deliveryFeeRepository = deliveryFeeRepository;
        }

        public IEnumerable<DeliveryFee> GetAllDeliveryFees()
            => _deliveryFeeRepository.List();

        public long GetFeeByPrice(long cartPrice)
            => _deliveryFeeRepository.List().FirstOrDefault(e => e.EligibleTransactionVolume.MinPrice <= cartPrice &&
                                                        (e.EligibleTransactionVolume.MaxPrice == null || e.EligibleTransactionVolume.MaxPrice > cartPrice))?.Price ?? 0;
    }
}
