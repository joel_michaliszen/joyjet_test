﻿namespace JoyJet.eCommerce.Core.DTO
{
    public class CartCheckoutDTO
    {
        public long Id { get; set; }
        public long Total { get; set; }
    }
}
