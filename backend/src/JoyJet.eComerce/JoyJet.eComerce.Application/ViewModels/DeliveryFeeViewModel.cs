namespace JoyJet.eComerce.Application.ViewModels
{
    public class DeliveryFeeViewModel
    {
        public long Id { get; set; }
        public long Price { get; set; }
        public EligibleTransactionVolumeViewModel EligibleTransactionVolume { get; set; }
    }
}