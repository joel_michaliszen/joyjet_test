﻿using System;
using System.Data.Entity;

namespace JoyJet.eCommerce.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        public DbContext Context { get; }

        private DbContextTransaction _transaction;
        private bool _rolledback;

        public UnitOfWork(JJContext context)
        {
            Context = context;
            StartTransaction();
        }

        public void StartTransaction() => _transaction = Context.Database.BeginTransaction();

        public void Commit()
        {
            if (_rolledback) return;

            Context.SaveChanges();
            _transaction.Commit();
        }

        public void Save()
        {
            if (!_rolledback)
                Context.SaveChanges();
        }

        public void Rollback()
        {
            _transaction.Rollback();
            _rolledback = true;
        }

        public void Dispose()
        {
            if (!_rolledback)
                Commit();
            GC.SuppressFinalize(this);
        }
    }
}
