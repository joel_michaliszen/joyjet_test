﻿namespace JoyJet.eCommerce.Core.Models
{
    public enum DiscountType
    {
        Amount,
        Percentage
    }
}