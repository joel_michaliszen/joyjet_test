Click here to see the build and quickly test report:
> [![Build status](https://ci.appveyor.com/api/projects/status/bhdi3a643qonsf70/branch/master?svg=true)](https://ci.appveyor.com/project/JoelRamosM/joyjet-test/branch/master)

Click here to see the running api using swagger interface that make simple to test REST call:
> [Running API][1] 

[1]: http://joyjetecommerce.apphb.com/swagger/ui/index 

----------

## Backend test - All levels was done

## Usings:
>Note:  The solution was builded using VisualStudio 2015 Community and target to .Net Framework 4.5.2

### Tech and Packages
+ Asp.Net 4.5 MVC 5 and WebApi
+ C# 6
+ Entity Framework 6.1
+ Newtosoft JSON
+ NUnit
+ NSubstitute
+ NinjectMockingKernel
+ Ninject 
+ Automapper
+ Swagger

### Patterns and Convention
---------
+ Implements Repository 
+ Unit of Work
+ One transaction per request [TODO]
+ Trying to follow S.O.L.I.D. guideline
+ Simple disposition of folders and project, because the project size





