namespace JoyJet.eCommerce.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingDiscountToArticle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Discount",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ArticleId = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        Value = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Article", t => t.ArticleId, cascadeDelete: true)
                .Index(t => t.ArticleId);
            
            AddColumn("dbo.Article", "DiscountId", c => c.Long());
            CreateIndex("dbo.Article", "DiscountId");
            AddForeignKey("dbo.Article", "DiscountId", "dbo.Discount", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Article", "DiscountId", "dbo.Discount");
            DropForeignKey("dbo.Discount", "ArticleId", "dbo.Article");
            DropIndex("dbo.Discount", new[] { "ArticleId" });
            DropIndex("dbo.Article", new[] { "DiscountId" });
            DropColumn("dbo.Article", "DiscountId");
            DropTable("dbo.Discount");
        }
    }
}
