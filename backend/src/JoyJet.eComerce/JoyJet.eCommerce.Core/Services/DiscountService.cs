﻿using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Interfaces.Services;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.CrossCutting.Exceptions;
using System;
using System.Collections.Generic;

namespace JoyJet.eCommerce.Core.Services
{
    public class DiscountService : IDiscountService
    {
        private readonly IRepository<Discount> _discountRepository;
        private readonly IRepository<Article> _articleRepository;

        private readonly Dictionary<DiscountType, List<Action<Discount>>> _validations;

        public DiscountService(IRepository<Discount> discountRepository, IRepository<Article> articleRepository)
        {
            _discountRepository = discountRepository;
            _articleRepository = articleRepository;

            _validations = new Dictionary<DiscountType, List<Action<Discount>>>
            {
                {DiscountType.Percentage, new List<Action<Discount>> { ValidatePercentValue, ValidateArticleExists, ValidatePercentValueByArticle }  },
                {DiscountType.Amount, new List<Action<Discount>> { ValidaZeroOrNegativeAmountValue, ValidateArticleExists,ValidateAmountValueByArticle }  }
            };
        }

        public IEnumerable<Discount> GetAllDiscount()
            => _discountRepository.List();

        public Discount IncludeDiscount(Discount discount)
        {
            ValidateDiscount(discount);
            var includeDiscount = _discountRepository.Include(discount);
            PrepareArticle(includeDiscount);
            return includeDiscount;
        }

        private void PrepareArticle(Discount includeDiscount)
        {
            var article = _articleRepository.Find(includeDiscount.ArticleId);
            article.DiscountId = includeDiscount.Id;
            _articleRepository.Update(article);
        }

        private void ValidateDiscount(Discount discount)
        {
            if (discount.ArticleId <= 0)
                throw new JoyJetECommerceException("Discount must to have an article!");
            _validations[discount.Type].ForEach(e => e(discount));
        }

        private void ValidateArticleExists(Discount discount)
        {
            if (_articleRepository.Exists(discount.ArticleId))
                return;
            throw new JoyJetECommerceException("Article doesnt exists.");
        }


        private void ValidateAmountValueByArticle(Discount discount)
        {
            var article = _articleRepository.Find(discount.ArticleId);
            ValidateArticleAlreadyHasDiscount(article);
            if (article.Price < discount.Value)
                throw new JoyJetECommerceException($"Amount discont cannot be bigger than the Article price: {article.Price}!");
        }

        private void ValidatePercentValueByArticle(Discount discount)
        {
            var article = _articleRepository.Find(discount.ArticleId);
            ValidateArticleAlreadyHasDiscount(article);
        }

        private static void ValidateArticleAlreadyHasDiscount(Article article)
        {
            if (article.DiscountId.HasValue)
                throw new JoyJetECommerceException($"The article {article.Name} already has a discount!");
        }

        private static void ValidaZeroOrNegativeAmountValue(Discount discount)
        {
            if (discount.Value <= 0)
                throw new JoyJetECommerceException("Amount discount must to be bigger than zero!");
        }

        private static void ValidatePercentValue(Discount discount)
        {
            if (discount.Value > 100 || discount.Value <= 0)
                throw new JoyJetECommerceException("Discount must to be between 1 and 100!");
        }
    }
}
