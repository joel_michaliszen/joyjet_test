﻿namespace JoyJet.eCommerce.Core.Models
{
    public class EligibleTransactionVolume
    {
        public long? MinPrice { get; set; }
        public long? MaxPrice { get; set; }
    }
}