﻿using JoyJet.eComerce.Application.Profiles;
using Ninject.MockingKernel.NSubstitute;
using NUnit.Framework;

namespace JoyJet.eComerce.ApplicationTest
{
    public abstract class TestBase
    {
        protected NSubstituteMockingKernel _kernel;

        protected TestBase()
        {
            MapperStart.Configure();
            _kernel = new NSubstituteMockingKernel();
        }
        [SetUp]
        protected void SetupBase()
            => _kernel.Reset();
    }
}
