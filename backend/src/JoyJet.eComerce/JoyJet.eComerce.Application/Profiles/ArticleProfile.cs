﻿using AutoMapper;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eComerce.Application.Profiles
{
    public class ArticleProfile : Profile
    {
        public ArticleProfile()
        {
            CreateMap<Article, ArticleViewModel>();
        }
    }
}
