﻿using JoyJet.eCommerce.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JoyJet.eCommerce.Core.Models
{
    public class Order : EntityBase
    {
        public virtual ICollection<OrderItem> Items { get; set; }

        public DateTime CheckoutAt { get; set; }

        public bool DeliveryNextDay { get; set; }

        public long Total => Items?.Sum(x => x.Article.Price * x.Quantity) ?? 0;
    }
}
