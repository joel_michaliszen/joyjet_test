namespace JoyJet.eComerce.Application.ViewModels
{
    public enum DiscounTypeViewModel
    {
        Amount,
        Percentage
    }
}