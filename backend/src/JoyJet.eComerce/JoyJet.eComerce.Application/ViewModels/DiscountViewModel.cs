namespace JoyJet.eComerce.Application.ViewModels
{
    public class DiscountViewModel
    {
        public long ArticleId { get; set; }
        public string Type { get; set; }
        public long Value { get; set; }
    }
}