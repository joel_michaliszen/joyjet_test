﻿using JoyJet.eCommerce.Core.Models.Common;

namespace JoyJet.eCommerce.Core.Models
{
    public class CartItem : EntityBase
    {
        public long ArticleId { get; set; }
        public virtual Article Article { get; set; }
        public long CartId { get; set; }
        public virtual Cart Cart { get; set; }
        public long Quantity { get; set; }

        public long Price()
        {
            var price = Article.Price;
            if (!Article.DiscountId.HasValue)
                return price * Quantity;

            switch (Article.Discount.Type)
            {
                case DiscountType.Amount:
                    price = Article.Price - Article.Discount.Value;
                    break;
                case DiscountType.Percentage:
                    var discountFactor = (100 - Article.Discount.Value) * 0.01;
                    price = (long)(Article.Price * discountFactor);
                    break;
                default:
                    price = Article.Price;
                    break;
            }
            return price * Quantity;
        }
    }
}