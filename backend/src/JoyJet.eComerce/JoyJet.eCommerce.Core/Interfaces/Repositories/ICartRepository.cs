﻿using JoyJet.eCommerce.Core.Models;

namespace JoyJet.eCommerce.Core.Interfaces.Repositories
{
    public interface ICartRepository : IRepository<Cart>
    {
    }
}
