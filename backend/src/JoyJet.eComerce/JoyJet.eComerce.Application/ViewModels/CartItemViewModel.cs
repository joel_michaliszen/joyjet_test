namespace JoyJet.eComerce.Application.ViewModels
{
    public class CartItemViewModel
    {
        public long ArticleId { get; set; }
        public long Quantity { get; set; }
    }
}