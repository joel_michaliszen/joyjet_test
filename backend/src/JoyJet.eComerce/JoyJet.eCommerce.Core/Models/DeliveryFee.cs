﻿using JoyJet.eCommerce.Core.Models.Common;

namespace JoyJet.eCommerce.Core.Models
{
    public class DeliveryFee : EntityBase
    {
        public EligibleTransactionVolume EligibleTransactionVolume { get; set; }
        public long Price { get; set; }
    }
}
