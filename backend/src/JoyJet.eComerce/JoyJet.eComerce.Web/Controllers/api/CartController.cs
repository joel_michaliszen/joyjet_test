﻿using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace JoyJet.eComerce.Web.Controllers.api
{
    /// <summary>
    /// Api to get, create , update and get carts prepared to checkout
    /// </summary>
    [RoutePrefix("api/carts")]
    public class CartController : ApiController
    {
        private readonly ICartApplication _cartApplication;

        public CartController(ICartApplication cartApplication)
        {
            _cartApplication = cartApplication;
        }

        /// <summary>
        /// Get all carts
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CartViewModel> Get()
            => _cartApplication.GetCarts();

        /// <summary>
        /// Get all carts prepared to checkout
        /// </summary>
        /// <returns></returns>
        [Route("checkout")]
        public IEnumerable<CartCheckoutViewModel> GetCartsToCheckout()
            => _cartApplication.GetCartsToCheckout();

        /// <summary>
        /// Create new Cart
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        public CartViewModel Post(CartViewModel cart)
            => _cartApplication.AddCart(cart);

        /// <summary>
        /// Add new item to an existing cart
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        [Route("{id}/addItem")]
        public HttpResponseMessage PutAddItem(long id, CartItemViewModel cartItem)
        {
            _cartApplication.AddItemToCart(id, cartItem);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
