﻿using Ninject;
using Ninject.Modules;

namespace JoyJet.eComerce.IoC
{
    public static class IoCFactory
    {
        public static IKernel CreateKernel(params INinjectModule[] modules)
            => new StandardKernel(modules);
    }
}
