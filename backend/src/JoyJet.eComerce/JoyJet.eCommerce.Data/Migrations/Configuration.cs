using JoyJet.eCommerce.Core.Models;
using System.Linq;

namespace JoyJet.eCommerce.Data.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<JJContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(JJContext context)
        {
            if (!context.Set<Article>().Any())
                context.Set<Article>().AddRange(new[]
                {
                new Article
                {
                    Name = "water",
                    Price = 100
                },
                new Article
                {
                    Name = "honey",
                    Price = 200
                },
                new Article
                {
                    Name = "mango",
                    Price = 400
                },
                new Article
                {
                    Name = "tea",
                    Price = 1000
                },
            });
            if (!context.Set<DeliveryFee>().Any())
                context.Set<DeliveryFee>().AddRange(new[]
                {
                    new DeliveryFee
                    {
                        Price = 800,
                        EligibleTransactionVolume = new EligibleTransactionVolume()
                        {
                            MinPrice = 0,
                            MaxPrice = 1000
                        }
                    },
                    new DeliveryFee
                    {
                        Price = 400,
                        EligibleTransactionVolume = new EligibleTransactionVolume()
                        {
                            MinPrice = 1000,
                            MaxPrice = 2000
                        }
                    },
                    new DeliveryFee
                    {
                        Price = 0,
                        EligibleTransactionVolume = new EligibleTransactionVolume()
                        {
                            MinPrice = 2000,
                            MaxPrice = null
                        }
                    },
                });

            context.SaveChanges();
        }
    }
}
