﻿namespace JoyJet.eComerce.Application.ViewModels
{
    public class ArticleViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long Price { get; set; }
    }
}
