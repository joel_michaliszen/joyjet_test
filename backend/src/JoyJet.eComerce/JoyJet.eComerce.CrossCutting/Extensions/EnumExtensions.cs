﻿using System;
using System.Linq;

namespace JoyJet.eCommerce.CrossCutting.Extensions
{
    public static class EnumExtensions
    {
        public static TEnum ToEnum<TEnum>(this string str)
        {
            var matchedEnum = typeof(TEnum).GetFields().FirstOrDefault(f => f.Name.Equals(str, StringComparison.CurrentCultureIgnoreCase));
            if (matchedEnum == null)
                throw new InvalidCastException($"Cannot convert to string {typeof(TEnum).Name}.");
            return (TEnum)Enum.Parse(typeof(TEnum), matchedEnum.Name);
        }
    }
}