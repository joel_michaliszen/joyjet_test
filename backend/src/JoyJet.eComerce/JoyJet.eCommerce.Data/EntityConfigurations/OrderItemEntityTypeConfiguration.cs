﻿using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Data.EntityConfigurations.Common;

namespace JoyJet.eCommerce.Data.EntityConfigurations
{
    public class OrderItemEntityTypeConfiguration : EntityBaseEntityTypeConfiguration<OrderItem>
    {
        public OrderItemEntityTypeConfiguration()
        {
            Property(e => e.Quantity);
            HasRequired(e => e.Order).WithMany(e => e.Items).HasForeignKey(e => e.OrderId);
            HasRequired(e => e.Article).WithMany().HasForeignKey(e => e.ArticleId);
        }
    }
}
