﻿using JoyJet.eCommerce.Core.Interfaces.Repositories;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.Core.Services;
using Ninject;
using NSubstitute;
using NUnit.Framework;

namespace JoyJet.eComerce.CoreTest
{
    [TestFixture]
    public class ArticleServiceTest : TestBase
    {
        private ArticleService _sut;
        private IRepository<Article> _articleRepository;

        [SetUp]
        protected void Setup()
        {
            _articleRepository = _kernel.Get<IRepository<Article>>();
            _sut = _kernel.Get<ArticleService>();
        }

        [Test]
        public void OnCallGetAllArticlesMustToCallListOfRepository()
        {
            _sut.GetAllArticles();
            _articleRepository.Received().List();
        }

        [Test]
        public void OnCallAddArticleMustToCallIncludeFromRepository()
        {
            var expected = new Article
            {
                Name = "Test",
                Price = 800
            };
            _sut.AddArticle(expected);
            _articleRepository.Received().Include(expected);
        }
    }
}
