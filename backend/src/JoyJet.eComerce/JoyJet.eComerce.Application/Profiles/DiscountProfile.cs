using AutoMapper;
using JoyJet.eComerce.Application.ViewModels;
using JoyJet.eCommerce.Core.Models;
using JoyJet.eCommerce.CrossCutting.Extensions;

namespace JoyJet.eComerce.Application.Profiles
{
    public class DiscountProfile : Profile
    {
        public DiscountProfile()
        {
            CreateMap<DiscountViewModel, Discount>()
                .ForMember(to => to.Type, f => f.MapFrom(from => from.Type.ToEnum<DiscountType>()));
            CreateMap<Discount, DiscountViewModel>()
                .ForMember(to => to.Type, f => f.MapFrom(from => from.Type.ToString()));
        }
    }
}