﻿using JoyJet.eCommerce.Core.Models.Common;

namespace JoyJet.eCommerce.Core.Models
{
    public class Discount : EntityBase
    {
        public long ArticleId { get; set; }
        public virtual Article Article { get; set; }
        public DiscountType Type { get; set; }
        public long Value { get; set; }
    }
}
