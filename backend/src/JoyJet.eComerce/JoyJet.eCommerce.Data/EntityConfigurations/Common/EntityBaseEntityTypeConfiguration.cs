﻿using JoyJet.eCommerce.Core.Models.Common;
using System.Data.Entity.ModelConfiguration;

namespace JoyJet.eCommerce.Data.EntityConfigurations.Common
{
    public abstract class EntityBaseEntityTypeConfiguration<T> : EntityTypeConfiguration<T> where T : EntityBase
    {
        protected EntityBaseEntityTypeConfiguration()
        {
            HasKey(e => e.Id);
        }
    }
}