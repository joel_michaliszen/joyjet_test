﻿using JoyJet.eComerce.Application.Interfaces;
using JoyJet.eComerce.Application.ViewModels;
using System.Collections.Generic;
using System.Web.Http;

namespace JoyJet.eComerce.Web.Controllers.api
{
    /// <summary>
    /// Api to get all Articles
    /// </summary>
    [RoutePrefix("api/articles")]
    public class ArticlesController : ApiController
    {
        private readonly IArticleApplication _articleApplication;

        public ArticlesController(IArticleApplication articleApplication)
        {
            _articleApplication = articleApplication;
        }

        public IEnumerable<ArticleViewModel> Get()
            => _articleApplication.GettAllArticles();
    }
}
